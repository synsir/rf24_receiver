#include <synos.h>

//#include "../stmbase/drivers/tm1638.h"
#include "../stmbase/drivers/nrf24/rf24.h"
//#include "../stmbase/drivers/ds18b20/ds18b20.h"

//#include <stdio.h>
//#define printf(x, y) (x, y)
//#define printf(x) (x)

syn::Timer1 tim1;
syn::Timer2 tim2;
// uses port b5 (I2C)
syn::Led alive_led;
uint8_t msg_buffer[32];

syn::Gpio beeper;
syn::DeadlineTimer beeper_timer;
volatile uint16_t beeper_millis;

volatile bool connection_was_made = false;
syn::DeadlineTimer con_lost_timer;


void beep(uint16_t ms, bool synchrone = true)
{
  beeper.clear();
  if(synchrone)
  {
    syn::sleep(ms);
    beeper.set();
  }
  else
  {
    beeper_millis = ms;
    beeper_timer.reset();
  }
}

void beeper_tick()
{
  if(beeper_timer.expired(beeper_millis))
  {
    beeper.set();
  }
}

void good_beep()
{
  beep(50);
  syn::sleep(50);
  beep(50);
}


struct Message_t
{
  uint32_t counter;
  uint8_t protocol;
  uint8_t reserved;
  uint16_t channels[9];
  uint8_t mic[8];
};

void write_pwms()
{
  alive_led.toggle();
  Message_t *pmsg = (Message_t*)msg_buffer;
  uint16_t* pwmvals = pmsg->channels;
  // set pwm values in their order
  tim1.setPWM(*pwmvals++, 3); // pwm 1
  tim1.setPWM(*pwmvals++, 4); // pwm 2
  tim2.setPWM(*pwmvals++, 1); // pwm 3
    pwmvals++;
  tim2.setPWM(*pwmvals++, 3); // pwm 5
  tim2.setPWM(*pwmvals++, 2); // pwm 6
}

void connection_lost_pwm()
{
  // tim 1
  tim1.setPWM(1500, 3); // pwm 1 to middle
  tim1.setPWM(1500, 4); // pwm 2
  // tim 2
  tim2.setPWM(1500, 1); // pwm 3
  tim2.setPWM(1500, 3); // pwm 5
  tim2.setPWM(1500, 2); // pwm 6
}

void pwm_control_routine(uint16_t arg)
{
  alive_led.init();
  alive_led.clear();
  beeper.init('A', 3)
    .opendrain()
    .set();

  
  tim1.init_rcpwm();
  tim1.enablePWM(8 + 4); // enable channel 3 and 4 -> RC channel 1 & 2
  tim2.init_rcpwm();
  tim2.enablePWM(4 + 2 + 1); // enable channel 1, 2, 3 -> RC channel 3, 6, 5
  
  connection_lost_pwm();

  good_beep();

  RF24::init("ReCv");
  RF24::set_destination("ReMo");
  RF24::listen();

  while(true)
  {
    int8_t size = RF24::read_mic(msg_buffer);
    if(size == 32)
    {
      good_beep();
      write_pwms();
      break;
    }
    else if(size == -1)
    {
      beep(250);
    }
    else if(size != 0)
    {
      beep(100);
    }else
    {
      syn::sleep(10);
    }
  }
  
  con_lost_timer.reset();
  connection_was_made = true;
  while (1)
  {
    int8_t size = RF24::read_mic(msg_buffer);
    if(size == 32)
    {
      // message received and mic correct
      write_pwms();
      con_lost_timer.reset();
    }

    syn::Routine::yield();
  }
} 

bool last_beeped;
syn::DeadlineTimer con_lost_beep_timer;

void con_lost_tick()
{
  if(con_lost_timer.expired(500))
  {
    // put pwms into safe position
    connection_lost_pwm();
    // make angry beeps
    if(con_lost_beep_timer.expired(333))
    {
      con_lost_beep_timer.reset();
      if(last_beeped)
      {
        last_beeped = false;
      }
      else
      {
        last_beeped = true;
        beep(333, false);
      }
    }
  }
}

void background_tasks(uint16_t arg)
{
  syn::Rate rate(15);

  // only start running background stuff after connection to TX
  while(connection_was_made == false)
  {
    rate.sleep();
  }

  last_beeped = false;
  con_lost_beep_timer.reset();
  while (1)
  {
    beeper_tick();
    con_lost_tick();
    rate.sleep();
  }
}

int main()
{
  syn::Kernel::init();

  // IMPORTANT !!!
  // the count of routines has to exactly match the amount of routines used
  // the routine index given during initialization has to match

  syn::Routine::init(&pwm_control_routine, 0, 200);
  syn::Routine::init(&background_tasks, 0, 200);

  // never returns
  syn::Kernel::spin();
  while(true)
  {
  }
}
